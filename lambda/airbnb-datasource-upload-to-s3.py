from time import sleep
from urllib import request
import boto3
import os
import json


client = boto3.client("s3")
source_data_url = "http://data.insideairbnb.com/the-netherlands/north-holland/amsterdam/2022-06-05/data/"

def handle_download(bucket, name, url):
    
    print(f'try opening {url}')
    file_name = os.path.basename(url)
    print(f'filename is  {file_name}')
    
    res = request.urlopen(url)
    
    print(res.status)
    print(res.reason)

    client.upload_fileobj(
        Fileobj=res,
        Bucket=bucket,
        Key="{}/{}".format(name, file_name),
    )

def lambda_handler(event, context):
    bucket = 'my-airbnb-bucket'

    handle_download(bucket, 'source_data/listings', source_data_url +'listings.csv.gz')
    sleep(2)
    handle_download(bucket, 'source_data/calendars', source_data_url +'calendar.csv.gz')

    return {'statusCode': 200}