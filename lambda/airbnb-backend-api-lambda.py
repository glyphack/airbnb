from typing import List, Tuple
import json
import time
import boto3
import pymysql
import logging
import redis
from datetime import timedelta
from redis.commands.json.path import Path
import os

log = logging.getLogger()
log.setLevel(logging.DEBUG)
redis_endpoint = os.environ['redis_endpoint']

# Connect to DB
print("Trying to connect...")
try:
    conn = pymysql.connect(
        host=os.environ['db_host'],
        user=os.environ['db_user'],
        passwd=os.environ['db_pass'],
        db=os.environ['db_name'],
    )
except pymysql.MySQLError as e:
    print("ERROR: Unexpected error: Could not connect to MySQL instance.")
    print(e)
    raise

PAGE_SIZE = 10

def room_availabilty(check_in: str,
                          check_out: str,
                          area: str,
                          page: int,
                          ) -> Tuple[int, List[dict]]:
    """
    Return the rooms availability result.
    page: indicate pagination.
    """
    
    total = 0
    result = None
    
    start_at= (page - 1) * PAGE_SIZE
    
    params = {'check_in': check_in, 'check_out': check_out}

    query = """
    SElECT 
        calendar_id,
        calendars1.listing_id,
        calendars1.cal_price,
        calendars1.minimum_nights,
        calendars1.maximum_nights,
        count(listing_id) as available_days, 
        abs(datediff(%(check_in)s, %(check_out)s)) as total_days
    FROM calendars1 
    WHERE 
        available = 't' 
        AND cal_date between %(check_in)s and %(check_out)s 
        GROUP BY listing_id HAVING count(listing_id) > abs(datediff(%(check_in)s, %(check_out)s))
        ORDER BY calendars1.cal_price asc
    """
    
    

    with conn.cursor() as cur:
        try:
            total = cur.execute(query, params)
            
            query += """ LIMIT %(start_at)s, %(page_size)s """
            params = {'check_in': check_in, 'check_out': check_out, 'start_at': start_at, 'page_size': PAGE_SIZE}
            
            cur.execute(query, params)
            result = cur.fetchall()
        except Exception as e:
            print('Unexpected error: could not write data to db:\n', e)
    
    return (total, result,)

def lambda_handler(event, context):
    
    total = 0
    result = None
    redis_conn = None
    key = ''
    
    # test
    if 'queryStringParameters' not in event:
        event['queryStringParameters'] = {"check_in": '2022-06-05', 'check_out':'2022-06-07', 'page': 1, 'area':''}
        
    check_in = str(event['queryStringParameters'].get('check_in'))
    check_out = str(event['queryStringParameters'].get('check_out'))
    area = str(event['queryStringParameters'].get('area'))
    page = int(event['queryStringParameters'].get('page', 1))
    
    
    try:
        event.get('queryStringParameters') 
        event['queryStringParameters'].get('check_in')
        event['queryStringParameters'].get('check_out')
    except KeyError:
        response = {
            "message": "check_in and check_out should be provided!"
        }
        
        return {
            'statusCode': 200,
            'body':  json.dumps(response),
        }
        
    try:
        log.debug('#Starting redis connection')
        redis_conn = redis.Redis.from_url('redis://' + redis_endpoint)
        if redis_conn.ping():
            log.debug('connected')
            key = 'a_' + check_in + '_' + check_out + "_" + str(page)
                
            # read from cache
            response = redis_conn.json().get(key)
            
            # if not set, set cache
            if not response:
                total, result = room_availabilty(
                    check_in,
                    check_out,
                    area,
                    page,
                )
                
                records = []
            
                for item in result:
                    records.append({
                        "calendar_id": item[0],
                        "listing_id": item[1],
                        "price": item[2],
                        "minimum_nights": item[3],
                        "maximum_nights": item[4],
                    })
                
                response = {
                    "total":total,
                    "result":  records,
                }
        
                redis_conn.json().set(key, Path.root_path(), response)
                redis_conn.expire(key, timedelta(seconds=30))  
    except Exception as ex:
        log.debug("failed in ", ex.__class__)
        return {
            'statusCode': 500
        }
    finally:
        del redis_conn
        
    
    return {
        'statusCode': 200,
        'body':  json.dumps(response),
    }
