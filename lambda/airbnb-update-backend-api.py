from calendar import calendar
from typing import List, Tuple
import json
import time
import boto3
import pymysql
import base64
from botocore.exceptions import ClientError
import os
        
# Connect to DB
print("Trying to connect...")
try:
    conn = pymysql.connect(
        host=os.environ['db_host'],
        user=os.environ['db_user'],
        passwd=os.environ['db_pass'],
        db=os.environ['db_name'],
    )
except pymysql.MySQLError as e:
    print("ERROR: Unexpected error: Could not connect to MySQL instance.")
    print(e)
    raise


def update_calendar(calendar_id : int, action: str) -> Tuple[int]:
    """
    Book calendar based on the calendar_id.
    """
    
    total = 0
    
    params = {'calendar_id': calendar_id}

    query = """
    SElECT 
       *
    FROM calendars1 
    WHERE 
        available = 'f' 
        AND calendar_id = %(calendar_id)s
    """


    with conn.cursor() as cur:
        try:
            total = cur.execute(query, params)
            result = cur.fetchOne()
            
            availability = 'f' if action == 'book' else 't'
            params = {'calendar_id': calendar_id, 'avaliability': availability}
            
            if(result):
                query = """ UPDATE calendars1 SET available = %(availability)s WHERE calendar_id = %(calendar_id)s """
                total = cur.execute(query, params)
            else:
                total = 0
        except Exception as e:
            print('Unexpected error: could not write data to db:\n', e)
    
    return (total)

def lambda_handler(event, context):
    
    total = 0
    result = None
    
    # just for testing lambda
    if 'queryStringParameters' not in event:
        event['queryStringParameters'] = {}

    calendar_id = int(event['queryStringParameters'].get('calender_id', 0))
    action = str(event['queryStringParameters'].get('action', ''))
    
    if calendar_id == 0 or action == '':
        response = {
            "message": "calendar_id and action should be provided!"
        }
        
        return {
            'statusCode': 200,
            'body':  json.dumps(response),
        }
    
    total= update_calendar(
       calendar_id,
        action,

    )
    
    book_message = f'You successfully book calendar_id: {calendar_id}' if action == 'book' and total == 1 else f'We cannot book calendar_id: {calendar_id}'
    cancel_message = f'You successfully cancel calendar_id: {calendar_id}' if action == 'cancel' and total == 1 else f'We cannot cancel calendar_id: {calendar_id}'

    
    response = {
        "total":total,
        "message" : book_message if action == 'book' else cancel_message
    }
    
    return {
        'statusCode': 200,
        'body': json.dumps(response),
    }
