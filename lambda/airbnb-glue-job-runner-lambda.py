import sys
import boto3
from botocore.exceptions import ClientError
import json
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def lambda_handler(event, context):
    
    glueJobName = "airbnb-glue-job"
    glue_client = boto3.client(service_name='glue', region_name='eu-west-1')
    
    try:
        logging.info('Doing something')
        job_run_id = glue_client.start_job_run(JobName=glueJobName)
        print(job_run_id['JobRunId'])

    except ClientError:
        print('Error while trying to run glue job')
        
    # TODO implement
    return {
        'statusCode': 200,
        'body': json.dumps('Glue Job Successfully Started!')
    }
